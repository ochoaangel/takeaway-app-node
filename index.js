var _ = require("underscore");
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var fs = require("fs");
var cors = require("cors");
app.use(cors());

//Variables
// var urlBase = "https://takeaway-cool-app-backend.herokuapp.com";
var urlBase = "http://localhost:3000"; 

var colorGenerated =
  "  --ion-color-primary: #f15a24;--ion-color-primary-rgb: 241,90,36;--ion-color-primary-contrast: #000000;--ion-color-primary-contrast-rgb: 0,0,0;--ion-color-primary-shade: #d44f20;--ion-color-primary-tint: #f26b3a;--ion-color-secondary: #ff921e;--ion-color-secondary-rgb: 255,146,30;--ion-color-secondary-contrast: #000000;--ion-color-secondary-contrast-rgb: 0,0,0;--ion-color-secondary-shade: #e0801a;--ion-color-secondary-tint: #ff9d35;--ion-color-tertiary: #640101;--ion-color-tertiary-rgb: 100,1,1;--ion-color-tertiary-contrast: #ffffff;--ion-color-tertiary-contrast-rgb: 255,255,255;--ion-color-tertiary-shade: #580101;--ion-color-tertiary-tint: #741a1a;--ion-color-success: #2dd36f;--ion-color-success-rgb: 45,211,111;--ion-color-success-contrast: #ffffff;--ion-color-success-contrast-rgb: 255,255,255;--ion-color-success-shade: #28ba62;--ion-color-success-tint: #42d77d;--ion-color-warning: #ffc409;--ion-color-warning-rgb: 255,196,9;--ion-color-warning-contrast: #000000;--ion-color-warning-contrast-rgb: 0,0,0;--ion-color-warning-shade: #e0ac08;--ion-color-warning-tint: #ffca22;--ion-color-danger: #eb445a;--ion-color-danger-rgb: 235,68,90;--ion-color-danger-contrast: #ffffff;--ion-color-danger-contrast-rgb: 255,255,255;--ion-color-danger-shade: #cf3c4f;--ion-color-danger-tint: #ed576b;--ion-color-dark: #000000;--ion-color-dark-rgb: 0,0,0;--ion-color-dark-contrast: #ffffff;--ion-color-dark-contrast-rgb: 255,255,255;--ion-color-dark-shade: #000000;--ion-color-dark-tint: #1a1a1a;--ion-color-medium: #92949c;--ion-color-medium-rgb: 146,148,156;--ion-color-medium-contrast: #ffffff;--ion-color-medium-contrast-rgb: 255,255,255;--ion-color-medium-shade: #808289;--ion-color-medium-tint: #9d9fa6;--ion-color-light: #eeeeee;--ion-color-light-rgb: 238,238,238;--ion-color-light-contrast: #000000;--ion-color-light-contrast-rgb: 0,0,0;--ion-color-light-shade: #d1d1d1;--ion-color-light-tint: #f0f0f0;";
var categories = [
  {
    id: "1",
    name: "bebidas",
    img: urlBase + "/Images/bebidas/250x250/png/250x250_1-1.png",
    imgh: urlBase + "/Images/bebidas/1080x1920/png/1080x1920_1-1.png",
    color: "#f21146",
  },
  {
    id: "2",
    name: "burger",
    img: urlBase + "/Images/burger/250x250/png/250x250_2-1.png",
    imgh: urlBase + "/Images/burger/1080x1920/png/1080x1920_2-1.png",
    color: "#9094fc",
  },
  {
    id: "3",
    name: "ensalada",
    img: urlBase + "/Images/ensaladas/250x250/png/250x250_3-1.png",
    imgh: urlBase + "/Images/ensaladas/1080x1920/png/1080x1920_3-1.png",
    color: "#99e663",
  },
  {
    id: "4",
    name: "pizza",
    img: urlBase + "/Images/pizza/250x250/png/250x250_4-1.png",
    imgh: urlBase + "/Images/pizza/1080x1920/png/1080x1920_4-1.png",
    color: "#96aafa",
  },
  {
    id: "5",
    name: "postres",
    img: urlBase + "/Images/postres/250x250/png/250x250_5-1.png",
    imgh: urlBase + "/Images/postres/1080x1920/png/1080x1920_5-1.png",
    color: "#945682",
  },
];

var productosBebidas = [
  {
    id: "11",
    name: "margarita",
    img: urlBase + "/Images/bebidas/250x250/png/250x250_1-1.png",
    imgh: urlBase + "/Images/bebidas/1080x1920/png/1080x1920_1-1.png",
    color: "#64c0fa",
  },
  {
    id: "12",
    name: "Caipiriña",
    img: urlBase + "/Images/bebidas/250x250/png/250x250_1-2.png",
    imgh: urlBase + "/Images/bebidas/1080x1920/png/1080x1920_1-2.png",
    color: "#72e085",
  },
  {
    id: "13",
    name: "Martini",
    img: urlBase + "/Images/bebidas/250x250/png/250x250_1-3.png",
    imgh: urlBase + "/Images/bebidas/1080x1920/png/1080x1920_1-3.png",
    color: "#6f5da1",
  },
  {
    id: "14",
    name: "Cuba Libre",
    img: urlBase + "/Images/bebidas/250x250/png/250x250_1-4.png",
    imgh: urlBase + "/Images/bebidas/1080x1920/png/1080x1920_1-4.png",
    color: "#f0bd71",
  },
];

var productosBurgers = [
  {
    id: "21",
    name: "Bomba",
    img: urlBase + "/Images/burger/250x250/png/250x250_2-1.png",
    imgh: urlBase + "/Images/burger/1080x1920/png/1080x1920_2-1.png",
    color: "#72e085",
  },
  {
    id: "22",
    name: "Sensación",
    img: urlBase + "/Images/burger/250x250/png/250x250_2-2.png",
    imgh: urlBase + "/Images/burger/1080x1920/png/1080x1920_2-2.png",
    color: "#6f5da1",
  },
  {
    id: "23",
    name: "Tunning",
    img: urlBase + "/Images/burger/250x250/png/250x250_2-3.png",
    imgh: urlBase + "/Images/burger/1080x1920/png/1080x1920_2-3.png",
    color: "#64c0fa",
  },
  {
    id: "24",
    name: "Todo Terreno",
    img: urlBase + "/Images/burger/250x250/png/250x250_2-4.png",
    imgh: urlBase + "/Images/burger/1080x1920/png/1080x1920_2-4.png",
    color: "#f0bd71",
  },
];

var productosEnsaladas = [
  {
    id: "31",
    name: "César",
    img: urlBase + "/Images/ensaladas/250x250/png/250x250_3-1.png",
    imgh: urlBase + "/Images/ensaladas/1080x1920/png/1080x1920_3-1.png",
    color: "#6f5da1",
  },
  {
    id: "32",
    name: "Clásica",
    img: urlBase + "/Images/ensaladas/250x250/png/250x250_3-2.png",
    imgh: urlBase + "/Images/ensaladas/1080x1920/png/1080x1920_3-2.png",
    color: "#64c0fa",
  },
  {
    id: "33",
    name: "Picoteri",
    img: urlBase + "/Images/ensaladas/250x250/png/250x250_3-3.png",
    imgh: urlBase + "/Images/ensaladas/1080x1920/png/1080x1920_3-3.png",
    color: "#f0bd71",
  },
  {
    id: "34",
    name: "Rayú",
    img: urlBase + "/Images/ensaladas/250x250/png/250x250_3-4.png",
    imgh: urlBase + "/Images/ensaladas/1080x1920/png/1080x1920_3-4.png",
    color: "#72e085",
  },
];

var productosPizza = [
  {
    id: "41",
    name: "margarita",
    img: urlBase + "/Images/pizza/250x250/png/250x250_4-1.png",
    imgh: urlBase + "/Images/pizza/1080x1920/png/1080x1920_4-1.png",
    color: "#6f5da1",
  },
  {
    id: "42",
    name: "Capressa",
    img: urlBase + "/Images/pizza/250x250/png/250x250_4-2.png",
    imgh: urlBase + "/Images/pizza/1080x1920/png/1080x1920_4-2.png",
    color: "#f0bd71",
  },
  {
    id: "43",
    name: "Bacon",
    img: urlBase + "/Images/pizza/250x250/png/250x250_4-3.png",
    imgh: urlBase + "/Images/pizza/1080x1920/png/1080x1920_4-3.png",
    color: "#72e085",
  },
  {
    id: "44",
    name: "Primavera",
    img: urlBase + "/Images/pizza/250x250/png/250x250_4-4.png",
    imgh: urlBase + "/Images/pizza/1080x1920/png/1080x1920_4-4.png",
    color: "#64c0fa",
  },
];

var productosPostres = [
  {
    id: "51",
    name: "Cup-Cake",
    img: urlBase + "/Images/postres/250x250/png/250x250_5-1.png",
    imgh: urlBase + "/Images/postres/1080x1920/png/1080x1920_5-1.png",
    color: "#f0bd71",
  },
  {
    id: "52",
    name: "Tarta",
    img: urlBase + "/Images/postres/250x250/png/250x250_5-2.png",
    imgh: urlBase + "/Images/postres/1080x1920/png/1080x1920_5-2.png",
    color: "#64c0fa",
  },
  {
    id: "53",
    name: "Pie",
    img: urlBase + "/Images/postres/250x250/png/250x250_5-3.png",
    imgh: urlBase + "/Images/postres/1080x1920/png/1080x1920_5-3.png",
    color: "#6f5da1",
  },
  {
    id: "54",
    name: "Smooth",
    img: urlBase + "/Images/postres/250x250/png/250x250_5-4.png",
    imgh: urlBase + "/Images/postres/1080x1920/png/1080x1920_5-4.png",
    color: "#72e085",
  },
];

var AllProducts = [
  ...productosBebidas,
  ...productosBurgers,
  ...productosEnsaladas,
  ...productosPizza,
  ...productosPostres,
];

// dando acceso a Imagenes   http://localhost:3000/Images/Inmuebles/a01.jpg
app.use(express.static("public"));
app.use("/Images", express.static(__dirname + "/Images"));

// para procesamiento post
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Preparando el puerto
var server = app.listen(process.env.PORT || 3000, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log("Example app listening at :%s", port);
  console.log("Example app listening at :%s", port);
  console.log("Example app listening at :%s", port);
  console.log("Example app listening at :%s", port);
  console.log("Example app listening at :%s", port);
  console.log("Example app listening at :%s", port);
  console.log(
    __dirname + " oooo " + host + " Example app listening at: %s",
    port
  );
  console.log("Example app listening at :%s", port);
});

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// api default
app.post("/api/v1.0/test", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = { ok: true, payload: "", message: "conexión satisfactoria" };
    res.status(200).send(fin);
  } catch (error) {
    res.status(200).send({ ok: false, message: "Incoherencia en datos " });
  }
});

app.post("/api/v1.0/getAppInfo", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  let uid = req.body.uid;
  if (req.body.uid) {
    try {
      let payload = {
        id: "35486516846535435",
        uid,
        takeAwayClient: "532685765",
        takeAwayOrgUnit: "87354654",
        nombre: "X-Appname-X",
        direccion: "X-AppDirection-X",
        email: "ejemplo@ejemplo.com",
        tel: "+5074528965412",
        menu_config: "X-menu_config-X",
        restaurant_config: "X-restaurant_config-X",
        images: "X-images-X",
        style_config: colorGenerated,
        fontsCollection: [
          { name: 'Open Sans', rol:'title'},
          { name: 'Montserrat', rol:'paragraph'},
          { name: 'Poppins', rol:'price'},
          { name: 'Petit Formal Script', rol:'other1'},
          { name: 'Nova Script', rol:'other2'}
        ]

        // fonts: [
        //   "Open Sans",
        //   "Montserrat",
        //   "Poppins",
        //   "Petit Formal Script",
        //   "Nova Script",
        // ],
      };

      var fin = { ok: true, payload, message: "conexión satisfactoria" };
      res.status(200).send(fin);
    } catch (error) {
      res
        .status(200)
        .send({ ok: false, payload: null, message: "Incoherencia en datos " });
    }
  } else {
    res
      .status(200)
      .send({ ok: false, payload: null, message: "Incoherencia en datos " });
  }
});

app.post("/api/v1.0/getCategories", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = {
      ok: true,
      payload: categories,
      message: "conexión satisfactoria",
    };
    res.status(200).send(fin);
  } catch (error) {
    res
      .status(200)
      .send({ ok: false, payload: null, message: "Incoherencia en datos " });
  }
});

app.post("/api/v1.0/getProducts", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = {
      ok: true,
      payload: [
        ...productosBebidas,
        ...productosBurgers,
        ...productosEnsaladas,
        ...productosPizza,
        ...productosPostres,
      ],
      message: "conexión satisfactoria",
    };
    res.status(200).send(fin);
  } catch (error) {
    res
      .status(200)
      .send({ ok: false, payload: null, message: "Incoherencia en datos " });
  }
});

app.post("/api/v1.0/getProduct", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");

  try {
    let payload = [];

    if (req.body.id) {
      var fin = {
        ok: true,
        payload: AllProducts.filter((res8a) => res8a.id == req.body.id)[0],
        message: "conexión satisfactoria",
      };
      res.status(200).send(fin);
    } else {
      var fin = { ok: false, payload: "", message: "conexión satisfactoria" };
      res.status(200).send(fin);
    }
  } catch (error) {
    res
      .status(200)
      .send({ ok: false, payload: null, message: "Incoherencia en datos " });
  }
});

/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////

// api default
app.get("/listUsers", function (req, res) {
  fs.readFile(__dirname + "/" + "users.json", "utf8", function (err, data) {
    console.log(data);
    console.log("consulta hecha.." + _.now());
    res.end(data);
  });
  res.status(200).send({ jfjfjf: "dffffffffffffffffffffffffffffffffffff" });
});

// api imagenes  http://localhost:8081/image?id=0
app.get("/image", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = _.findWhere(inmuebles, { id: parseInt(req.query.id) });
    res.status(200).send(fin); // responde array de datos de ese inmuebles
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// api imagenes  http://localhost:8081/image?id=0
app.get("/inmuebles", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    // var fin = _.findWhere(inmuebles, { id: parseInt(req.query.id) });
    // var fin = inmuebles
    res.status(200).send(inmuebles); // responde array de datos de ese inmuebles
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// api user trae toda la inf del usuario  http://192.168.16.106:8081/user?id=3
app.get("/user", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = _.findWhere(user, { id: parseInt(req.query.id) });
    res.status(200).send(fin); // responde array de todos los datos de ese usuario
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// confirmar usuario y pass http://localhost:8081/confirm?user=user03&pass=pass03
app.get("/confirm", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    myfilter = { user: req.query.user, pass: req.query.pass };
    var fin = _.where(user, myfilter);
    res.status(200).send(fin); // responde array con todos los datos de ese usuario
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// AGREGAR usuario COMPLETO http://localhost:8081/adduser?user=usernn&pass=passnn&rol=P&phone=phonenn
app.get("/adduser", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    myfilter = { user: req.query.user };
    var fin = _.where(user, myfilter);
    if (fin.length === 0) {
      //{ id: 0, user: "user00", pass: "pass00", rol: "P", phone: "phone00" },

      var mi_usuario = {
        id: user.length,
        user: req.query.user,
        pass: req.query.pass,
        rol: req.query.rol,
        phone: req.query.phone,
      };
      let antes = user.length;
      user.push(mi_usuario);
      let despues = user.length;
      res.status(200).send({ error: false, message: "Registrado" });
      console.log(
        "Agregado un usuario, antes habian " + antes + " ahora hay " + despues
      );
    } else {
      res.status(200).send({
        error: true,
        message: "Usuario No registrado, faltan datos" + fin.length,
      });
    }
  } catch (error) {
    res.status(200).send({
      error: true,
      message: "Incoherencia en datos " + error.message + fin.length,
    });
  }
});

// borra un usuario  http://localhost:8081/deleteuser?id=1
app.get("/deleteuser", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var fin = _.findWhere(user, { id: parseInt(req.query.id) });

    //  arr = _.without(arr, _.findWhere(arr, { id: 3 }));
    var finSinUsuario = _.without(
      user,
      _.findWhere(user, { id: parseInt(req.query.id) })
    );
    user = finSinUsuario;
    console.log(user);
    res.status(200).send(fin); // responde array de todos los datos de ese usuario
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// registra un usuario  http://192.168.16.106:8081/register?user=usuarioo&pass=clave&phone=mitlf
app.get("/register", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  try {
    var persona = {
      id: user.length,
      user: req.query.user,
      pass: req.query.pass,
      rol: "I",
      phone: req.query.phone,
      active: true,
    };
    console.log(JSON.stringify(persona));

    myfilter = { user: req.query.user };
    var fin = _.where(user, myfilter);

    if (fin.length !== 0) {
      // no sepuede registrar, ya existe
      console.log("registrado ya existe..");
      res.status(200).send({ error: true, message: "ya existe el  usuario" }); // responde array de todos los datos de ese usuario
    } else {
      // proceder a registrar
      user.push(persona);
      console.log(JSON.stringify(user));
      console.log("registrado usuario nuevo..");
      res.status(200).send({
        error: false,
        message: "registrado nuevo usuario     ahora:" + user.length,
      }); // responde array de todos los datos de ese usuario
    }
  } catch (error) {
    res.status(200).send({ error: true, message: "Incoherencia en datos " });
  }
});

// POST http://localhost:8081/api/users
// parameters sent with
app.get("/api/users", function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.type("application/json");
  res.send("holaaaaaaaaaaaaaa");
});
